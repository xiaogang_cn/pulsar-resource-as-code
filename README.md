# GitOps with Apache Pulsar resource

## Why?
- Single source of truth for sepcific Pulsar resources
- Automation for higher efficiency
- Traceability for record and easier troubleshooting
- Access control and code review approval in SCM for compliance and quality
- Best practice sharing across organization

## Prerequisites
- A GitLab server with [KAS](https://docs.gitlab.com/ee/administration/clusters/kas.html) enabled
- A k8s cluster which can access the GitLab server
- An [Apache Pulsar](https://pulsar.apache.org) cluster accessible from the k8s cluster
- [Pulsar Resource Operator](https://github.com/streamnative/pulsar-resources-operator) installed to the k8s cluster

## Steps to follow

- Createa a [manifest file](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#create-an-agent-configuration-file) for GitLab Kubernetes agent conf
- [Register and install the GitLab Kubernetes agent](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html) with above agent conf
- Create [manifest files for Pulsar resources](https://github.com/streamnative/pulsar-resources-operator/tree/main/docs)
- [Update the agent conf](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html#gitops-configuration-reference) to point to the manifest files of the Pulsar resource
- When the resouce manifest files are updated the reconcilation will happen within 5 minutes to update the resources in Pulsar.

## Reference links

- [Introducing Pulsar Resources Operator for Kubernetes](https://streamnative.io/blog/release/2022-08-15-introducing-pulsar-resources-operator-for-kubernetes/)
- [Using GitOps from GitLab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html)
